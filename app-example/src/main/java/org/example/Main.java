package org.example;

import org.example.client.nb.no.api.SearchApi;
import org.example.client.nb.no.invoker.ApiClient;
import org.example.client.nb.no.model.Pageable;
import org.example.client.nb.no.model.SuperItemSearchEntityModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.origin.SystemEnvironmentOrigin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
public class Main {
  public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
  }

  @Bean
  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    return args -> {
      SearchApi client = new SearchApi();
      SuperItemSearchEntityModel obj = client
        .superSearch("car", new Pageable().page(0), null, null, null, null, null,null,null,null,null, null,null,null,null,null);
      System.out.println(obj.toString());
    };
  }
}

